/**
 *
 * @param element {HTMLElement}
 * @param onMove {DragMoveCallback}
 */
export function addDragListener(element, onMove)
{
    let lastX = null, lastY = null, dragging = false;
    element.draggable = false;

    /**
     * @param e {MouseEvent}
     */
    function start(e)
    {
        dragging = true;
        lastX = e.clientX;
        lastY = e.clientY;
    }

    function stop()
    {
        dragging = false;
    }

    /**
     * @param e {MouseEvent}
     */
    function move(e)
    {
        if(!dragging) return;
        const deltaX = e.clientX - lastX;
        const deltaY = e.clientY - lastY;
        onMove(deltaX, deltaY);
        lastX = e.clientX;
        lastY = e.clientY;
    }

    element.addEventListener('mousedown', start);
    document.body.addEventListener('mousemove', move);
    document.body.addEventListener('mouseup', stop);
    document.body.addEventListener('mouseleave', stop);

}

/**
 * @callback DragMoveCallback
 * @param {number} deltaX
 * @param {number} deltaY
 */