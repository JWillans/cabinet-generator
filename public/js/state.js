export class State
{

    persistName = null;

    /**
     * @type {Object.<string, *>}
     */
    state = {};

    /**
     * @type {StateChangeCallback[]}
     */
    listeners = [];

    /**
     * @return {string|null}
     */
    getLocalStorageKey()
    {
        if(!this.persistName) return null;
        return `state_${this.persistName}`
    }

    /**
     * @param [triggerUpdate] {boolean}
     */
    load(triggerUpdate)
    {
        const key = this.getLocalStorageKey();
        if(!key) return;
        const json = window.localStorage.getItem(key);
        if(json === null) return;
        this.state = JSON.parse(json);
        if(triggerUpdate) this.triggerUpdate();
    }

    save()
    {
        const key = this.getLocalStorageKey();
        if(!key) return;
        const json = JSON.stringify(this.state);
        window.localStorage.setItem(key, json);
    }

    /**
     * @param [initialState] {Object.<string, *>}
     * @param [onChange] StateChangeCallback|null
     * @param [persistName] {string}
     */
    constructor(initialState, onChange, persistName)
    {
        this.persistName = persistName;
        this.state = {...(initialState ?? {})};
        this.load(false);
        if(onChange) this.addListener(onChange);
    }

    /**
     * @param {Object.<string, *>} stateUpdate
     */
    set(stateUpdate)
    {
        this.state = {...this.state, ...stateUpdate};
        this.save();
        this.triggerUpdate();
    }

    /**
     * @param key {string}
     * @return *
     */
    getProperty(key)
    {
        return this.state[key];
    }

    /**
     * @param key {string}
     * @param value {*}
     */
    setProperty(key, value)
    {
        this.set({[key]: value});
    }

    /**
     * @return {Object.<string, *>}
     */
    get()
    {
        return this.state;
    }

    /**
     * @param listener {StateChangeCallback}
     */
    addListener(listener)
    {
        this.listeners.push(listener);
    }

    triggerUpdate()
    {
        this.listeners.forEach(listener => {
            listener(this.state);
        })
    }

}

/**
 * @param [initialState] {Object.<string, *>}
 * @param [onChange] StateChangeCallback|null
 * @param [persistName] {string}
 */
export function createState(initialState, onChange, persistName)
{
    return new State(initialState, onChange, persistName);
}

/**
 * @callback StateChangeCallback
 * @param {Object.<string, *>} state
 */