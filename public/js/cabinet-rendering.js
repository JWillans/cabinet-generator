import {Vector3} from "./vendor/three.module.js";
import {CabinetRenderer} from "./ThreeJS/CabinetRenderer.js";

/**
 * @param x {number}
 * @param y {number}
 * @param z {number}
 * @return {Vector3}
 */
export function createVector3(x, y, z)
{
    return new Vector3(x, y, z);
}

/**
 *
 * @param x {number}
 * @param y {number}
 * @param z {number}
 * @return {Vector3}
 */
export function v3(x, y, z)
{
    return new Vector3(x, y, z);
}

/**
 * @param origin {Vector3}
 * @param offset {Vector3}
 * @return {Vector3}
 */
export function offsetVector3(origin, offset)
{
    return origin.clone().add(offset);
}

/**
 * @param type {CabinetComponentType}
 * @param id {string}
 * @param dimensions {Vector3}
 * @param position {Vector3}
 * @param [rotation] {Vector3}
 * @param [description] {string|undefined}
 * @return {CabinetComponent}
 */
export function createComponent(type, id, dimensions, position, rotation, description)
{
    return {
        type: type,
        id: id,
        description: description ?? null,
        position: position,
        dimensions: dimensions,
        rotation: rotation ?? createVector3(0, 0, 0),
    }
}

/**
 * @param component {CabinetComponent}
 * @param shrinkAmount {number}
 * @return {CabinetComponent}
 */
export function shrinkComponent(component, shrinkAmount)
{
    return createComponent(
        component.type,
        `${component.id}_shrunk_${shrinkAmount}`,
        createVector3(component.dimensions.x - shrinkAmount, component.dimensions.y - shrinkAmount, component.dimensions.z - shrinkAmount),
        offsetVector3(component.position, createVector3(shrinkAmount / 2, shrinkAmount / 2, shrinkAmount / 2)),
        component.rotation,
    );
}

export function calculateDividerComponents(box, panelThickness, idPrefix, numDividers)
{
    const dividerGap = (box.dimensions.x) / (numDividers + 1);
    const dividerDimensions = createVector3(panelThickness, box.dimensions.y, box.dimensions.z);

    let components = [];

    for(let i = 0; i < numDividers; i++){

        let x = box.position.x + dividerGap + (dividerGap * i) - (panelThickness / 2);

        components.push(createComponent(
            'panel',
            `${idPrefix}_divider_${i + 1}`,
            dividerDimensions.clone(),
            createVector3(x, box.position.y, box.position.z),
        ));

    }

    return components;
}

/**
 * @param box {CabinetComponent}
 * @param panelThickness {number}
 * @param idPrefix {string}
 * @param numShelves {number}
 * @param [skipShelfNumbers] {number}
 * @return {*[]}
 */
export function calculateShelfComponents(box, panelThickness, idPrefix, numShelves, skipShelfNumbers)
{
    skipShelfNumbers = skipShelfNumbers ?? [];

    const gap = 1;

    const shelfGap = (box.dimensions.y) / (numShelves + 1);
    const shelfDimensions = createVector3(box.dimensions.x - (gap * 2), panelThickness, box.dimensions.z - (gap * 2));

    let components = [];

    for(let i = 0; i < numShelves; i++){

        if(skipShelfNumbers.includes(i + 1)) continue;

        let y = box.position.y + shelfGap + (shelfGap * i) - (panelThickness / 2);

        components.push(createComponent(
            'panel',
            `${idPrefix}_shelf_${i + 1}`,
            shelfDimensions.clone(),
            createVector3(box.position.x + gap, y, box.position.z + gap),
        ));

    }

    return components;

}

/**
 * @typedef {Object} FrontOpenBoxOptions
 * @property {boolean} auto_split_back
 * @property {number} doors
 * @property {number} num_shelves
 * @property {number[]} skip_shelf_numbers
 */

/**
 *
 * @param box {CabinetComponent}
 * @param parameters {CabinetParameters}
 * @param idPrefix {string}
 * @param [options] {number}
 * @return {CabinetComponent[]}
 */
export function calculateFrontOpenBoxComponents(box, parameters, idPrefix, options)
{
    options = {
        auto_split_back: true,
        num_doors: 0,
        num_shelves: 0,
        skip_shelf_numbers: [],
        bottom_plate: true,
        ...(options ?? {})
    }

    // Auto split the back panel to allow better optimisation
    const splitBackX = options.auto_split_back ? Math.ceil(box.dimensions.x / 1200) : 1;
    const splitBackY = options.auto_split_back ? Math.ceil(box.dimensions.y / 1200) : 1;

    let components = [];

    if(options.bottom_plate){
        components.push(
            createComponent(
                'panel',
                `${idPrefix}_bottom`,
                createVector3(box.dimensions.x, parameters.panel_thickness, box.dimensions.z),
                box.position.clone(),
            )
        );
    }

    components.push(
        createComponent(
            'panel',
            `${idPrefix}_top`,
            createVector3(box.dimensions.x, parameters.panel_thickness, box.dimensions.z),
            createVector3(box.position.x, box.position.y + box.dimensions.y - parameters.panel_thickness, box.position.z),
        )
    );

    const sideDimensions = createVector3(parameters.panel_thickness, box.dimensions.y - (parameters.panel_thickness * 2) + (options.bottom_plate ? 0 : parameters.panel_thickness), box.dimensions.z);

    components.push(
        createComponent(
            'panel',
            `${idPrefix}_left`,
            sideDimensions.clone(),
            createVector3(
                box.position.x,
                box.position.y + (options.bottom_plate ? parameters.panel_thickness : 0),
                box.position.z
            ),
        )
    );

    components.push(
        createComponent(
            'panel',
            `${idPrefix}_right`,
            {...sideDimensions},
            createVector3(
                box.position.x + box.dimensions.x - parameters.panel_thickness,
                box.position.y + (options.bottom_plate ? parameters.panel_thickness : 0),
                box.position.z
            ),
        )
    );

    const backFullWidth = box.dimensions.x - (parameters.panel_thickness * 2);
    const backFullHeight = box.dimensions.y - (options.bottom_plate ? parameters.panel_thickness * 2 : parameters.panel_thickness);

    const backSplitWidth = backFullWidth / splitBackX;
    const backSplitHeight = backFullHeight / splitBackY;

    const backDimensions = createVector3(backSplitWidth, backSplitHeight, parameters.panel_thickness);

    for(let ix = 0; ix < splitBackX; ix++){
        for(let iy = 0; iy < splitBackY; iy++){

            components.push(
                createComponent(
                    'panel',
                    `${idPrefix}_back_${ix + 1}_${iy + 1}`,
                    {...backDimensions},
                    createVector3(
                        box.position.x + parameters.panel_thickness + (backSplitWidth * ix),
                        box.position.y + (options.bottom_plate ? parameters.panel_thickness : 0) + (backSplitHeight * iy),
                        box.position.z
                    ),
                )
            );

        }
    }

    if(options.num_doors > 0){
        components = [
            ...components,
            ...calculateInsetFrontDoorComponents(
                box,
                idPrefix,
                {
                    panel_thickness: parameters.panel_thickness,
                    num_doors: options.num_doors,
                    inset_bottom: options.bottom_plate,
                }
            ),
        ];
    }

    if(options.num_shelves > 0){
        components = [
            ...components,
            ...calculateShelfComponents(shrinkComponent(box, parameters.panel_thickness * 2), parameters.panel_thickness, idPrefix, options.num_shelves, options.skip_shelf_numbers)
        ]
    }

    return components;
}

/**
 * @typedef {Object} InsetDoorOptions
 * @property {number} num_doors
 * @property {number} panel_thickness
 */

/**
 *
 * @param box CabinetComponent
 * @param idPrefix {string}
 * @param [options] {InsetDoorOptions}
 * @return {CabinetComponent[]}
 */
function calculateInsetFrontDoorComponents(box, idPrefix, options)
{
    options = {
        num_doors: 1,
        panel_thickness: 18,
        inset_bottom: true,
        gap: 2,
        ...(options ?? {})
    }

    const width = box.dimensions.x - (options.panel_thickness * 2) - (options.gap * 2);

    const fullHeight = box.dimensions.y - (options.inset_bottom ? options.panel_thickness * 2 : options.panel_thickness);
    const height = (fullHeight - (options.gap * (options.num_doors + 1))) / options.num_doors;

    const dimensions = createVector3(width, height, options.panel_thickness);

    let components = [];

    for(let i = 0; i < options.num_doors; i++){

        const position = createVector3(
            box.position.x + options.panel_thickness + options.gap,
            box.position.y + options.gap + (options.inset_bottom ? options.panel_thickness : 0) + (height * i) + (options.gap * i),
            box.position.z + box.dimensions.z - options.panel_thickness
        )

        components.push(
            createComponent(
                'panel',
                `${idPrefix}_door_${i + 1}`,
                {...dimensions},
                position
            )
        )

    }

    return components;

}

/**
 * @param component {CabinetComponent}
 * @param parameters {CabinetParameters}
 * @return {string}
 */
export function formatPanelComponentFlatDimensions(component, parameters)
{
    const dimensions = resolvePanelComponentFlatDimensions(component, parameters);
    return `${dimensions.width}mm × ${dimensions.length}mm`;
}

/**
 * @param component {CabinetComponent}
 * @param parameters {CabinetParameters}
 * @return {{width: number, length, number}}
 */
export function resolvePanelComponentFlatDimensions(component, parameters)
{
    let values = [component.dimensions.x, component.dimensions.y, component.dimensions.z];

    let depthFound = false;
    values = values.filter(value => {
        if(depthFound === false && value === parameters.panel_thickness){
            depthFound = true;
            return false;
        }
        return true;
    });

    return {
        width: values[0],
        length: values[1],
    }
}

/**
 * @param component {CabinetComponent}
 * @param numParts {number}
 * @return {CabinetComponent[]}
 */
export function splitComponentVertically(component, numParts)
{
    let components = [];

    const partDimensions = {
        ...component.dimensions,
        y: component.dimensions.y / numParts
    }

    for(let i = 0; i < numParts; i++){
        components.push({
            ...component,
            id: `${component.id}_${i + 1}`,
            dimensions: {...partDimensions},
            position: component.position.clone().setY(component.position.y + (partDimensions.y * i))
        })
    }

    return components;
}

/**
 * @param container {HTMLElement}
 * @param model {CabinetModel}
 * @param options {CabinetRendererOptions}
 * @return {CabinetRenderer}
 */
export function insertCabinetRenderer(container, model, options)
{
    return new CabinetRenderer(container, model, options);
}