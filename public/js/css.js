import {createElement} from "./dom.js";

const loadedCss = [];

export function loadCss(href)
{
    if(loadedCss.includes(href)) return;
    loadedCss.push(href);

    const link = createElement('link', {
        attrs: {
            rel: 'stylesheet',
            href: href
        }
    });

    document.head.append(link);

}