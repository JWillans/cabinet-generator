export class Collection
{

    /**
     * @param [values] {string[]}
     */
    constructor(values)
    {
        this.values = [...(values ?? [])];
    }

    add(value)
    {
        if(this.values.includes(value)) return;
        this.values.push(value);
    }

    remove(value)
    {
        const index = this.values.indexOf(value);
        if(index < 0) return;
        this.values.splice(index, 1);
    }

    has(value)
    {
        return this.values.indexOf(value) >= 0;
    }

    toggle(value)
    {
        if(this.values.includes(value)){
            this.remove(value);
        }
        else{
            this.add(value);
        }
    }

}