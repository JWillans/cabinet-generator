import {createElement, createTextElement} from "./dom.js";

/**
 * @param [children] {(Node|string)[]}
 * @param [onSubmit] {FormSubmitCallback}
 */
export function createForm(children, onSubmit)
{
    const form = createElement('form', {
        children: children
    });

    form.addEventListener('submit', (e) => {
        e.preventDefault();
        if(onSubmit !== undefined) onSubmit();
    })

    return form;
}

/**
 * @param name {string}
 * @param label {string}
 * @param widget {HTMLElement}
 * @return {HTMLElement}
 */
export function createFormRow(name, label, widget)
{
    if(!widget.hasAttribute('id')){
        widget.setAttribute('id', name);
    }

    return createElement('div.form-row', {children: [
        createElement('label', {attrs: {for: widget.getAttribute('id') ?? name}, text_content: label}),
        widget
    ]});
}

/**
 * @param {string} name
 * @param checked {boolean}
 * @param onChange {CheckboxChangeCallback}
 */
export function createCheckbox(name, checked, onChange)
{
    /**
     * @type {HTMLInputElement}
     */
    const element = createElement('input', {
        attrs: {
            type: 'checkbox',
            checked: checked,
            name: name,
        }
    });

    element.addEventListener('change', () => {
        onChange(element.checked);
    });

    return element;
}

/**
 * @param name {string}
 * @param value {string|null}
 * @param choices {SelectChoice[]}
 * @param onChange {SelectChangeCallback}
 * @return {HTMLElement}
 */
export function createSelect(name, value, choices, onChange)
{
    /**
     * @type {HTMLSelectElement}
     */
    const element = createElement('select');
    choices.forEach(choice => {
        element.append(createElement('option', {
            text_content: choice.label,
            attrs: {
                name: name,
                value: choice.value,
            }
        }))
    })
    element.value = value;
    element.addEventListener('change', () => {
        onChange(element.value);
    })
    return element;
}

export function createInput(name, type, value, onChange)
{
    /** @type {HTMLInputElement} */
    const element = createElement('input', {attrs: {type: type}});
    element.value = value;

    ['change', 'keyup'].forEach(eventName => {
        element.addEventListener(eventName, () => {
            let value = `${element.value}`;
            if(value === '') value = null;
            if(type === 'number') value = parseFloat(value ?? 0);
            onChange(value);
        });
    });

    return element;

}

/**
 * @param header {string[]}
 * @param rows {(string|number)[][]}
 * @return {HTMLElement}
 */
export function createSimpleDataTable(header, rows)
{
    const thead = createElement('thead', {children: [
            createElement('tr', {children: header.map(columnName => {
                    return createTextElement('th', columnName);
                })})
        ]});

    const tbody = createElement('tbody', {children: rows.map(row => {
            return createElement('tr', {children: row.map(value => {
                    return createElement('td', {children: [value]});
                })})
        })});

    return createElement('table.data-table', {children: [thead,tbody]});
}

/**
 * @param label {string}
 * @param onClick {Function}
 * @return {HTMLElement}
 */
export function createButton(label, onClick)
{
    return createElement('a.button', {
        text_content: label,
        attrs: {
            href: '#'
        },
        callback: (button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault();
                onClick();
            })
        }
    })
}

/**
 * @callback CheckboxChangeCallback
 * @param {boolean} checked
 */

/**
 * @callback FormSubmitCallback
 */

/**
 * @callback SelectChangeCallback
 * @param {string|null} value
 */

/**
 * @typedef SelectChoice
 * @property {string} label
 * @property {string} value
 */