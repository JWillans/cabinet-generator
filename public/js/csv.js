import {createElement} from "./dom.js";

export function exportCsv(rows, filename)
{
    const content = createCsv(rows);

    const link = createElement('a');
    link.download = filename;
    link.href = 'data:text/csv;base64,' + btoa(content);
    link.click();
}

export function createCsv(rows)
{
    let content = '';

    rows.forEach((row, rowIndex) => {
        if(rowIndex > 0) content += "\n";
        row.forEach((value, columnIndex) => {
            if(columnIndex > 0) content += ',';
            content += `${value}`;
        });
    });

    return content;

}