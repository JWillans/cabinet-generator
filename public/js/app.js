import {createElement, createTextElement} from "./dom.js";
import {formatPanelComponentFlatDimensions, insertCabinetRenderer, resolvePanelComponentFlatDimensions} from "./cabinet-rendering.js";
import {loadCss} from "./css.js";
import {createSimpleDataTable, createButton, createSelect} from "./ui.js";
import {exportCsv} from "./csv.js";

import {DeskSurroundModel} from "./models/DeskSurroundModel.js";

loadCss('/css/app.css');

/**
 * @type {HTMLElement[]}
 */
const initialisedElements = [];

function init()
{
    const elements = [...document.querySelectorAll('[data-cabinet-generator]')];
    elements.forEach(initElement);
}

/**
 * @param model {CabinetModel}
 */
function exportPanelsCsv(model)
{
    const parameters = model.state.get();
    const components = model.component_calculator(parameters);

    const panels = components.filter(component => component.type === 'panel');

    const header = ['Length', 'Width', 'Qty', 'Label', 'Enabled'];

    const rows = panels.map(panel => {
        const dimensions = resolvePanelComponentFlatDimensions(panel, parameters);
        return [dimensions.length, dimensions.width, 1, panel.id, 'true']
    });

    const date = new Date();
    const filename = `Panels_${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}_${date.getHours()}${date.getMinutes()}${date.getSeconds()}`

    exportCsv([header, ...rows], filename);

}

/**
 * @param element {HTMLElement}
 */
function initElement(element)
{
    if(initialisedElements.includes(element)) return;
    initialisedElements.push(element);

    element.classList.add('cabinet-generator');
    loadCss('/css/component/cabinet-generator.css');

    const model = new DeskSurroundModel({
        // Add options here...
    });

    // Set up DOM container elements

    const previewContainer = createElement('div.cabinet-generator--preview');
    element.append(previewContainer);

    const previewRenderContainer = createElement('div.cabinet-generator--preview--render');
    previewContainer.append(previewRenderContainer);

    const componentLabelElement = createElement('div.cabinet-generator--component-label');
    previewRenderContainer.append(componentLabelElement);

    const previewControlsContainer = createElement('div.cabinet-generator--preview--controls');
    previewContainer.append(previewControlsContainer);

    const panelListContainer = createElement('div.cabinet-generator--panel-list');
    element.append(panelListContainer);

    const exportContainer = createElement('div.cabinet-generator--export');
    element.append(exportContainer);

    // Render

    const renderer = insertCabinetRenderer(
        previewRenderContainer,
        model,
        {
            width: 1920,
            aspect_ratio: 4 / 3,
            projection: "perspective",
        }
    );

    const viewControlsContainer = createElement('div.cabinet-generator--view-controls', {}, (viewControlsContainer) => {

        viewControlsContainer.append(createButton('Front', () => {
            renderer.setRotationDeg(0, 0, 0);
        }));

        viewControlsContainer.append(createButton('Side', () => {
            renderer.setRotationDeg(0, 90, 0);
        }));

        viewControlsContainer.append(createButton('Top', () => {
            renderer.setRotationDeg(90, 0, 0);
        }));

        viewControlsContainer.append(createButton('Back', () => {
            renderer.setRotationDeg(0, 180, 0);
        }));

        viewControlsContainer.append(createSelect(
            'render_style',
            renderer.options.render_style,
            [
                {label: 'Solid', value: 'solid'},
                {label: 'Outline', value: 'outline'},
            ],
            renderer.setRenderStyle.bind(renderer),
        ));

        viewControlsContainer.append(createSelect(
            'projection',
            renderer.options.projection,
            [
                {label: 'Perspective', value: 'perspective'},
                {label: 'Orthographic', value: 'orthographic'},
            ],
            renderer.setProjection.bind(renderer),
        ));

        viewControlsContainer.append(createButton('Download image', () => {
            renderer.downloadScreenshot();
        }));

    });

    previewControlsContainer.append(viewControlsContainer);

    const parametersForm = model.createForm();
    parametersForm.classList.add('cabinet-generator--parameters');
    previewControlsContainer.append(parametersForm);

    exportContainer.append(
        createButton('Download Panels CSV', () => {
            exportPanelsCsv(model);
        }),
        createElement('p.center-text', {
            html_content: 'Create plywood cutting plans on <a href="https://www.cutlistoptimizer.com/" target="_blank">cutlistoptimizer.com</a>',
        })
    );

    // State Listeners

    model.state.addListener(() => {
        panelListContainer.replaceChildren(renderPanelTable(model));
    });

    // GO!

    model.state.triggerUpdate();

}

/**
 * @param model {CabinetModel}
 */
function renderPanelTable(model)
{
    const components = model.calculateComponents();

    return createSimpleDataTable(
        ['Type', 'ID', 'Dimensions', 'Description'],
        components
            .filter(component => component.type !== 'placeholder')
            .map(component => {
                return [
                    component.type,
                    component.id,
                    (
                        component.type === 'panel' ?
                        formatPanelComponentFlatDimensions(component, model.state.get()) :
                        `${component.dimensions.x}mm × ${component.dimensions.y}mm × ${component.dimensions.z}mm`
                    ),
                    component.description ?? '',
                ]
            })
    );

}

init();

/**
 * @typedef {Object.<string, *>} CabinetParameters
 */

/**
 * @typedef {Object} Vector3
 * @property {number} x
 * @property {number} y
 * @property {number} z
 */

/**
 * @typedef {"panel"|"attachment"|"info"|"placeholder"} CabinetComponentType
 */

/**
 * @typedef {Object} CabinetModel
 * @property parameters {CabinetParameters}
 * @property state {State}
 * @property form {HTMLFormElement}
 * @property component_calculator {CabinetComponentCalculator}
 */

/**
 * @typedef {Object} CabinetComponent
 * @property {CabinetComponentType} type
 * @property {string} id
 * @property {string|null} description
 * @property {Vector3} position
 * @property {Vector3} dimensions
 * @property {Vector3} rotation
 */

/**
 * @callback CabinetComponentCalculator
 * @param Object.{string, *} parameters
 * @return CabinetComponent[]
 */