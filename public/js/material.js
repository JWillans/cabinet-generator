import {Color} from "./vendor/three.module.js";

export class ColourCycler
{

    colours = [
        new Color(1,0,0),
        new Color(0,1,0),
        new Color(0,0,1),
        new Color(0,1,1),
        new Color(1,0,1),
        new Color(1,1,0),
    ];

    index = 0;

    reset()
    {
        this.index = 0;
    }

    nextColour()
    {
        const colour = this.colours[this.index];
        this.index++;
        if(this.index >= this.colours.length) this.index = 0;
        return colour;
    }

}