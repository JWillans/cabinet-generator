/**
 * @param tagName {string}
 * @param [options] {ElementOptions}
 * @param [callback] {ElementCallback}
 * @return {HTMLElement}
 */
export function createElement(tagName, options, callback)
{
    options = {
        ...DEFAULT_ELEMENT_OPTIONS,
        ...(options ?? {})
    };

    if(callback !== undefined){
        options.callback = callback;
    }

    const tagNameParts = tagName.split('.');
    if(tagNameParts.length === 0) throw 'No tag name provided';

    const element = document.createElement(tagNameParts[0]);

    tagNameParts.slice(1).forEach(className => {
        element.classList.add(className);
    });

    if(options.attrs !== null){
        Object.keys(options.attrs).forEach(key => {
            const value = options.attrs[key];
            if(value === false) return;
            element.setAttribute(key, value);
        })
    }

    if(options.html_content) element.innerHTML = options.html_content;

    if(options.text_content) element.textContent = options.text_content;

    if(options.children !== null && options.children !== undefined) element.append(...options.children);

    if(options.callback) options.callback(element);

    return element;
}

/**
 * @param tagName {string}
 * @param textContent {string}
 * @param [options] {ElementOptions|undefined}
 */
export function createTextElement(tagName, textContent, options)
{
    if(options === undefined) options = {...DEFAULT_ELEMENT_OPTIONS};
    options.text_content = textContent;
    return createElement(tagName, options);
}

export function createDebugElement(data)
{
    const json = JSON.stringify(data, null, 4);

    return createElement('pre', {
        text_content: json,
    })
}



/**
 * @type {ElementOptions}
 */
const DEFAULT_ELEMENT_OPTIONS = {
    text_content: null,
    html_content: null,
    children: null,
    callback: null,
    attrs: null,
}

/**
 * @typedef {Object} ElementOptions
 * @property {string|null} [text_content]
 * @property {(Node|string)[]|null} [children]
 * @property {ElementCallback|null} [callback]
 * @property {Object.<string, string|boolean>|null} [attrs]
 */

/**
 * @callback ElementCallback
 * @param {HTMLElement} element
 */