import {Group, Mesh, MeshBasicMaterial, SphereGeometry} from "../vendor/three.module.js";
import {DebugMarker} from "./DebugMarker.js";

export class TextLabel extends Group
{

    constructor(text, width, height) {
        super();

        this.add(new DebugMarker()); // @debug

        this.init();
    }

    init()
    {
        // @todo
    }

}