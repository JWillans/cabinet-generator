import {
    AmbientLight,
    BoxGeometry,
    Color,
    DirectionalLight,
    EdgesGeometry,
    Group,
    LineBasicMaterial,
    LineSegments,
    Mesh,
    MeshBasicMaterial,
    MeshPhongMaterial,
    OrthographicCamera,
    PCFSoftShadowMap,
    PerspectiveCamera,
    Raycaster, RGBAFormat,
    Scene,
    Vector2,
    Vector3,
    WebGLRenderer
} from "../vendor/three.module.js";

import {ColourCycler} from "../material.js";
import {createVector3, shrinkComponent} from "../cabinet-rendering.js";
import {addDragListener} from "../event.js";
import {MeasurementLine} from "./MeasurementLine.js";
import {createElement, createTextElement} from "../dom.js";

export const COMPONENT_SCALE = 0.001; // mm -> ThreeJS unit (1m = 1 unit)

/**
 * @typedef {Object} CabinetRendererOptions
 * @property on_mouse_over_object {Function|null}
 * @property on_mouse_leave_object {Function|null}
 * @property on_mouse_move {Function|null}
 * @property projection {"perspective"|"orthographic"}
 * @property shadows {boolean}
 * @property render_style {"solid"|"outline"}
 * @property width {number}
 * @property aspect_ratio {number}
 * @property auto_hide_measurements {boolean}
 * @property background_colour {Color}
 * @property placeholder_component_colour {Colour}
 */

/**
 * @type {CabinetRendererOptions}
 */
export const DEFAULT_CABINET_RENDERER_OPTIONS = {
    width: 1080,
    aspect_ratio: 16 / 9,
    on_mouse_over_object: null,
    on_mouse_leave_object: null,
    on_mouse_move: null,
    projection: "perspective", // perspective / orthographic
    shadows: false,
    render_style: "outline", // solid / outline
    auto_hide_measurements: true,
    background_colour: new Color(0.75, 0.75, 0.75),
    placeholder_component_colour: new Color(0.8, 0.8, 0.8),
}

export class CabinetRenderer
{

    /** @type {HTMLElement} */
    componentLabelElement;

    /** @type {CabinetRendererOptions} */
    options;

    /** @type {Scene} */
    scene;

    /** @type {Camera} */
    camera;

    /** @type {WebGLRenderer} */
    renderer;

    /** @type {Group} */
    cabinetGroup;

    /** @type {Vector2} */
    mousePointer;

    /** @type {Raycaster} */
    raycaster;

    /** @type {{
     *      hitbox_meshes: Object.<string, Mesh>,
     *      measurement_groups: Object.<string, Group>
     *  }} */
    objects;

    /** @type {HTMLElement} */
    container;

    /** @type {CabinetModel} */
    model;

    /** @type {ColourCycler} */
    colourCycler;

    /** @type {Object3D|null} */
    hoverObject = null;

    /**
     * @param container {HTMLElement}
     * @param model {CabinetModel}
     * @param options {CabinetRendererOptions}
     */
    constructor(container, model, options)
    {
        this.container = container;
        this.container.classList.add('cabinet-generator--preview--render');

        this.componentLabelElement = createElement('div.cabinet-generator--preview--component-label');

        this.model = model;

        this.options = {
            ...DEFAULT_CABINET_RENDERER_OPTIONS,
            ...(options ?? {})
        };

        this.mousePointer = new Vector2(-1, -1);
        this.raycaster = new Raycaster();
        this.colourCycler = new ColourCycler();

        this.initScene();
        this.initCamera();
        this.initRenderer();
        this.initContainer();

        this.clearObjects();

        this.model.state.addListener(() => {
            this.updateComponents();
        });

        this.addLighting();
        this.enableMouseControls();
        this.updateComponents();

        if(this.options.shadows) this.enableShadows();

        this.getCanvas().classList.add('cabinet-generator--preview--render--canvas');

        this.render();
    }

    showComponentLabel(id, dimensions)
    {
        this.componentLabelElement.replaceChildren(
            createTextElement('span', id),
            createTextElement('small', dimensions),
        );
        this.componentLabelElement.classList.add('cabinet-generator--preview--component-label-visible');
    }

    hideComponentLabel()
    {
        this.componentLabelElement.classList.remove('cabinet-generator--preview--component-label-visible');
    }

    forEachHitboxMesh(callback)
    {
        Object.keys(this.objects.hitbox_meshes).forEach(id => {
            callback(this.objects.hitbox_meshes[id], id);
        });
    }

    forEachMeasurementGroup(callback)
    {
        Object.keys(this.objects.measurement_groups).forEach(id => {
            callback(this.objects.measurement_groups[id], id);
        });
    }

    initContainer()
    {
        this.container.replaceChildren(
            this.getCanvas(),
            this.componentLabelElement,
        );
    }

    enableShadows()
    {
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = PCFSoftShadowMap;
    }

    /**
     * @return {HTMLCanvasElement}
     */
    getCanvas()
    {
        return this.renderer.domElement;
    }

    clearObjects()
    {
        this.objects = {
            hitbox_meshes: {},
            measurement_groups: {},
        };

        if(this.cabinetGroup !== undefined){
            this.scene.remove(this.cabinetGroup);
        }

        this.cabinetGroup = new Group();
        this.scene.add(this.cabinetGroup);
    }

    addLighting()
    {
        const ambLight = new AmbientLight(0xffffff, 0.4);
        this.scene.add(ambLight);

        const light = new DirectionalLight(0xffffff, 2, 100);
        light.position.x = 0;
        light.position.y = 1;
        light.position.z = 1;

        light.castShadow = true;

        light.shadow.mapSize.width = 512;
        light.shadow.mapSize.height = 512;
        light.shadow.camera.near = 0.1;
        light.shadow.camera.far = 500;

        this.scene.add(light);
    }

    resetCamera()
    {
        const parameters = this.model.getParameters();
        const dimensions = parameters.dimensions ?? null;
        const z = dimensions ? (dimensions.z ?? 0) : 0;

        this.camera.position.x = 0;
        this.camera.position.y = 0;

        if(this.camera instanceof OrthographicCamera){
            this.camera.position.z = (z * COMPONENT_SCALE) * 10;
            this.camera.zoom = 0.3;
            this.camera.updateProjectionMatrix();
        }
        else if(this.camera instanceof PerspectiveCamera){
            this.camera.position.z = (z * COMPONENT_SCALE) * 4;
            this.camera.zoom = 1;
            this.camera.updateProjectionMatrix();
        }
    }

    /**
     * @param object {Object3D}
     */
    onMouseLeaveObject(object)
    {
        if(this.options.auto_hide_measurements) this.hideMeasurementGroups();

        this.forEachHitboxMesh(mesh => {
            meshEffect(mesh, null);
        });

        this.hideComponentLabel();

        if(this.options.on_mouse_leave_object){
            this.options.on_mouse_leave_object(object);
        }
    }

    /**
     * @param object {Object3D}
     */
    onMouseOverObject(object)
    {

        const id = object.userData.id;
        const dimensions = object.userData.dimensions;

        if(id !== undefined && this.options.auto_hide_measurements){
            this.hideMeasurementGroups();
            this.showMeasurementGroup(id);
        }

        if(id !== undefined){
            this.showComponentLabel(id, dimensions ?? '');
        }

        meshEffect(object, 'highlight');
        this.forEachHitboxMesh(mesh => {
            if(mesh.uuid === object.uuid) return;
            meshEffect(mesh, 'faded');
        });

        if(this.options.on_mouse_over_object){
            this.options.on_mouse_over_object(object);
        }

    }

    render()
    {
        this.raycaster.setFromCamera(this.mousePointer, this.camera);

        const hitboxMeshes = Object.keys(this.objects.hitbox_meshes).map(key => this.objects.hitbox_meshes[key]);
        const intersects = this.raycaster.intersectObjects(hitboxMeshes);

        if(intersects.length > 0){
            const intersectObject = intersects[0].object;
            if(this.hoverObject === null || (this.hoverObject && this.hoverObject.uuid !== intersectObject.uuid)){
                if(this.hoverObject) this.onMouseLeaveObject(this.hoverObject);
                this.hoverObject = intersectObject;
                this.onMouseOverObject(this.hoverObject);
            }
        }
        else if(this.hoverObject){
            this.onMouseLeaveObject(this.hoverObject);
            this.hoverObject = null;
        }

        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(this.render.bind(this));
    }

    hideMeasurementGroups()
    {
        this.forEachMeasurementGroup(group => {
            group.visible = false;
        });
    }

    showMeasurementGroup(id)
    {
        const group = this.objects.measurement_groups[id];
        if(group === undefined) return;
        group.visible = true;
    }

    /**
     * @return {{width, height: number}}
     */
    getDimensions()
    {
        return {
            width: this.options.width,
            height: this.options.width / this.options.aspect_ratio
        }
    }

    /**
     * @param x {number}
     * @param y {number}
     * @param z {number}
     */
    setRotation(x, y, z)
    {
        this.cabinetGroup.rotation.x = x;
        this.cabinetGroup.rotation.y = y;
        this.cabinetGroup.rotation.z = z;
    }

    /**
     * @param x {number}
     * @param y {number}
     * @param z {number}
     */
    setRotationDeg(x, y, z)
    {
        const degRad = Math.PI / 180;

        this.setRotation(
            x * degRad,
            y * degRad,
            z * degRad,
        );
    }

    initScene()
    {
        this.scene = new Scene();
        this.scene.userData = {};
        this.scene.background = this.options.background_colour;
    }

    initCamera()
    {
        const projection = this.options.projection;
        const dimensions = this.getDimensions();

        let camera;

        if(projection === 'orthographic'){
            const x = 1;
            const y = dimensions.height / dimensions.width;
            const scale = 0.7;
            camera = new OrthographicCamera(-(x * scale), x * scale, y * scale, -(y * scale), 0.1, 1000);
        }
        else if(projection === 'perspective'){
            camera = new PerspectiveCamera(60, dimensions.width / dimensions.height, 0.1, 1000);
        }
        else{
            throw `Unsupported projection "${projection}"`
        }

        this.camera = camera;
        this.resetCamera();
    }

    initRenderer()
    {
        this.renderer = new WebGLRenderer({
            antialias: true,
            preserveDrawingBuffer: true,
        });

        const dimensions = this.getDimensions();
        this.renderer.setSize(dimensions.width, dimensions.height);

        this.renderer.domElement.removeAttribute('style');
    }

    setProjection(projection)
    {
        this.options.projection = projection;
        this.initCamera();
    }

    setRenderStyle(renderStyle)
    {
        this.options.render_style = renderStyle;
        this.updateComponents();
    }

    updateComponents()
    {
        // Remember rotation before we do anything
        const oldCabinetGroupRotation = this.cabinetGroup.rotation.clone();

        this.colourCycler.reset();
        this.clearObjects();

        // Restore remembered rotation
        this.cabinetGroup.rotation.copy(oldCabinetGroupRotation);

        const parameters = this.model.getParameters();

        const components = this.model.calculateComponents()
            .filter(component => component.type !== 'info')
            .map(component => {
                // Transform component positions so that origin is in the center of the geometry
                return {
                    ...component,
                    position: createVector3(
                        component.position.x - (parameters.dimensions.x / 2),
                        component.position.y - (parameters.dimensions.y / 2),
                        component.position.z - (parameters.dimensions.z / 2),
                    )
                }
            })
        ;

        components.forEach(component => {
            const object = this.transformComponentToObject(component);
            const userData = object.userData;
            if(userData !== undefined && userData.hitboxMesh !== undefined){
                this.objects.hitbox_meshes[component.id] = object.userData.hitboxMesh;
            }
            this.cabinetGroup.add(object);
        });

        components.forEach(component => {
            const measurementGroup = createMeasurementGroup(component);
            if(this.options.auto_hide_measurements) measurementGroup.visible = false;
            this.objects.measurement_groups[component.id] = measurementGroup;
            this.cabinetGroup.add(measurementGroup);
        });

    }

    enableMouseControls()
    {

        const canvas = this.getCanvas();

        const MAX_X_ROTATION = 90 * (Math.PI / 180);
        const MIN_X_ROTATION = -90 * (Math.PI / 180);

        canvas.addEventListener('mousemove', (e) => {

            const width = canvas.clientWidth;
            const height = canvas.clientHeight;

            const x = e.layerX;
            const y = e.layerY;

            this.mousePointer.x = (x / width) * 2 - 1;
            this.mousePointer.y = -((y / height) * 2 - 1);

            if(this.options.on_mouse_move){
                this.options.on_mouse_move(this.mousePointer);
            }

        });

        canvas.addEventListener('wheel', (e) => {
            e.preventDefault();

            if(this.camera instanceof OrthographicCamera){
                const delta = -(e.deltaY * 0.001);
                this.camera.zoom *= 1 + delta;
                this.cabinetGroup.position.z = 0;
                this.camera.updateProjectionMatrix();
            }
            else if(this.camera instanceof PerspectiveCamera){
                const delta = -(e.deltaY * 0.0004);
                this.cabinetGroup.position.z += delta;
            }
        });

        addDragListener(canvas, (deltaX, deltaY) => {
            this.cabinetGroup.rotation.y += (deltaX * 0.005);
            this.cabinetGroup.rotation.x += (deltaY * 0.005);
            if(this.cabinetGroup.rotation.x < MIN_X_ROTATION) this.cabinetGroup.rotation.x = MIN_X_ROTATION;
            if(this.cabinetGroup.rotation.x > MAX_X_ROTATION) this.cabinetGroup.rotation.x = MAX_X_ROTATION;
        });

    }

    transformComponentToObject(component)
    {
        const renderStyle = this.options.render_style;

        const isHitbox = ['panel', 'attachment'].includes(component.type);

        /** @type {Object3D} */
        let object;

        const geometry = createComponentGeometry(component);

        if(renderStyle === "solid"){

            const colour = (component.type === 'placeholder') ? this.options.placeholder_component_colour : this.colourCycler.nextColour();

            const material = new MeshPhongMaterial({
                color: colour
            });

            object = new Mesh(geometry, material);
            addComponentUserdataToObject(object, component);
            if(isHitbox) object.userData.hitboxMesh = object;

            object.receiveShadow = true;
            object.castShadow = true;

        }
        else if(renderStyle === 'outline'){

            object = new Group()
            object.userData = {};
            addComponentUserdataToObject(object, component);

            // White Fill

            const fillGeometry = createComponentGeometry(shrinkComponent(component, 2));
            const fillMaterial = new MeshBasicMaterial({
                color: new Color(1, 1, 1)
            });

            const fillMesh = new Mesh(fillGeometry, fillMaterial);

            fillMesh.userData = {};
            addComponentUserdataToObject(fillMesh, component);

            object.add(fillMesh);
            if(isHitbox) object.userData.hitboxMesh = fillMesh;

            // Edges

            const edgeGeometry = new EdgesGeometry(geometry, 1);
            const edgeMaterial = new LineBasicMaterial({
                color: new Color(0, 0, 0),
                linewidth: 1,
            });

            const edgeLineSegments = new LineSegments(edgeGeometry, edgeMaterial);
            object.add(edgeLineSegments);


        }
        else{
            throw `Unsupported render style ${renderStyle}`;
        }

        const position = transformComponentPosition(component);

        object.position.x = position.x;
        object.position.y = position.y;
        object.position.z = position.z;

        return object;
    }

    downloadScreenshot()
    {
        const canvas = this.getCanvas();
        const context = canvas.getContext("experimental-webgl", {preserveDrawingBuffer: true});
        const link = document.createElement('a');

        link.download = generateScreenshotFilename(this.model);
        link.href = canvas.toDataURL('image/png');
        link.click();
    }

}

/**
 * @param model {CabinetModel}
 */
function generateScreenshotFilename(model)
{
    const date = new Date();

    const year = date.getFullYear();

    let month = date.getMonth() + 1;
    month = month < 10 ? `0${month}` : `${month}`;

    let day = date.getDate();
    day = day < 10 ? `0${day}` : `${day}`;

    let hours = date.getHours();
    hours = hours < 10 ? `0${hours}` : `${hours}`;

    let minutes = date.getMinutes();
    minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;

    return `${model.getName()}_${year}-${month}-${day}_${hours}${minutes}.png`;
}

/**
 * @param component {CabinetComponent}
 * @return {Vector3}
 */
function transformComponentPosition(component)
{
    const position = component.position;
    const dimensions = component.dimensions;

    const x = (position.x + (dimensions.x / 2)) * COMPONENT_SCALE;
    const y = (position.y + (dimensions.y / 2)) * COMPONENT_SCALE;
    const z = (position.z + (dimensions.z / 2)) * COMPONENT_SCALE;

    return new Vector3(x, y, z);
}


/**
 * @param component {CabinetComponent}
 * @return {Vector3}
 */
function transformComponentDimensions(component)
{
    const dimensions = component.dimensions;

    const x = dimensions.x * COMPONENT_SCALE;
    const y = dimensions.y * COMPONENT_SCALE;
    const z = dimensions.z * COMPONENT_SCALE;

    return new Vector3(x, y, z);
}

/**
 * @param component {CabinetComponent}
 * @return {BoxGeometry}
 */
function createComponentGeometry(component)
{
    return new BoxGeometry(
        component.dimensions.x * COMPONENT_SCALE,
        component.dimensions.y * COMPONENT_SCALE,
        component.dimensions.z * COMPONENT_SCALE,
    );
}

/**
 * @param object {Object3D}
 * @param component {CabinetComponent}
 */
function addComponentUserdataToObject(object, component)
{
    object.userData = {};

    object.userData.id = component.id;
    object.userData.type = component.type;
    object.userData.dimensions = `${component.dimensions.x}mm × ${component.dimensions.y}mm × ${component.dimensions.z}mm`;
}

/**
 * @param component {CabinetComponent}
 * @return {Group}
 */
function createMeasurementGroup(component)
{
    const group = new Group();

    const dimensions = transformComponentDimensions(component);
    const origin = transformComponentPosition(component);

    const bottomLeft = origin.sub(new Vector3(
        dimensions.x / 2,
        dimensions.y / 2,
        dimensions.z / 2
    ));

    group.position.copy(bottomLeft);

    group.add(new MeasurementLine(
        new Vector3(0, 0, 0),
        new Vector3(0, dimensions.y, 0),
    ));

    group.add(new MeasurementLine(
        new Vector3(0, dimensions.y, 0),
        new Vector3(dimensions.x, dimensions.y, 0),
    ));

    group.add(new MeasurementLine(
        new Vector3(0, dimensions.y, 0),
        new Vector3(0, dimensions.y, dimensions.z),
    ));

    return group;
}

/**
 * @param mesh {Object3D|Mesh}
 * @param effect {string|null}
 */
function meshEffect(mesh, effect)
{

    if(!mesh instanceof Mesh) return;

    const id = mesh.userData.id;
    const material = mesh.material;

    if(material.userData === undefined) material.userData = {};

    if(material.userData.originalColour === undefined) material.userData.originalColour = material.color;
    if(material.userData.originalOpacity === undefined) material.userData.originalOpacity = material.opacity;
    if(material.userData.originalTransparent === undefined) material.userData.originalTransparent = material.transparent;

    // Reset effects
    material.color = material.userData.originalColour;
    material.opacity = material.userData.originalOpacity;
    material.originalTransparent = material.userData.originalTransparent;

    if(material instanceof MeshPhongMaterial){

        // Reset effects
        if(material.userData.originalEmissive === undefined) material.userData.originalEmissive = material.emissive;
        material.emissive = material.userData.originalEmissive;

        if(effect === 'highlight'){
            material.emissive = new Color(0.5, 0.5, 0.5);
        }

    }

    if(material instanceof MeshBasicMaterial){

        if(effect === 'highlight'){
            material.color = new Color(0.5, 0.5, 1);
        }

    }

    if(material instanceof MeshPhongMaterial || material instanceof MeshBasicMaterial){

        // @todo get transparency effects to work
        if(effect === 'faded'){
            material.transparent = true;
            material.opacity = 0.5;
        }

    }

}
