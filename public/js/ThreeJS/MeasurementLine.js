import {Group, Vector3} from "../vendor/three.module.js";
import {Lines} from "./Lines.js";
import {TextLabel} from "./TextLabel.js";
import {COMPONENT_SCALE} from "./CabinetRenderer.js";
import {DebugMarker} from "./DebugMarker.js";

export class MeasurementLine extends Group
{

    /** @type {Lines} */
    lines;

    /** @type {number} */
    thickness;

    /** @type {Vector3} */
    start;

    /** @type {Vector3} */
    end;

    /**
     * @param start {Vector3}
     * @param end {Vector3}
     * @param [thickness] {number}
     */
    constructor(start, end, thickness)
    {
        super();

        this.lines = new Lines();
        this.thickness = thickness ?? 0.05;
        this.start = start;
        this.end = end;

        this.draw();
    }

    getFrontPoint()
    {
        return this.start.z > this.end.z ? this.start : this.end;
    }

    getBackPoint()
    {
        return this.start.z < this.end.z ? this.start : this.end;
    }

    getLeftPoint()
    {
        return this.start.x < this.end.x ? this.start : this.end;
    }

    getRightPoint()
    {
        return this.start.x > this.end.x ? this.start : this.end;
    }

    getTopPoint()
    {
        return this.start.y > this.end.y ? this.start : this.end;
    }

    getBottomPoint()
    {
        return this.start.y < this.end.y ? this.start : this.end;
    }

    getLengthMm()
    {
        return this.getLength() / COMPONENT_SCALE;
    }

    getLength()
    {
        return this.start.distanceTo(this.end);
    }

    calculateDimensions()
    {
        return new Vector3(
            Math.abs(this.start.x - this.end.x),
            Math.abs(this.start.y - this.end.y),
            Math.abs(this.start.z - this.end.z)
        );
    }

    /**
     * @return {"width"|"height"|"depth"}
     */
    getType()
    {
        const dimensions = this.calculateDimensions();
        if(dimensions.x > dimensions.y && dimensions.x > dimensions.z) return 'width';
        if(dimensions.z > dimensions.y && dimensions.z > dimensions.x) return 'depth';
        return 'height';
    }

    /**
     * @return {Vector3}
     */
    getLineOffset()
    {
        const type = this.getType();

        if(type === 'height'){
            return new Vector3(-(this.thickness / 2), 0, 0);
        }
        else if(type === 'width'){
            return new Vector3(0, this.thickness / 2, 0);
        }
        return new Vector3(-(this.thickness / 2), 0, 0);
    }

    draw()
    {
        this.clear();

        this.add(this.lines);

        const minLength = 0.05;

        const length = this.getLength();
        if(length < minLength) return;

        this.lines.clear();

        const type = this.getType();

        const offset = this.getLineOffset();

        const start = this.start.clone().add(offset);
        const end = this.end.clone().add(offset);
        const midpoint = calculateMidpoint(start, end);

        this.lines.drawLine(start, end);

        //const label = new TextLabel(`${this.getLengthMm()}mm`, this.thickness * 4, this.thickness);
        //const labelOffset = new Vector3(0, 0, 0);
        //this.add(label);

        if(type === 'height'){
            //labelOffset.add(new Vector3(-this.thickness, 0, 0));
            this.lines.drawTopArrow(this.getTopPoint().add(offset), this.thickness);
            this.lines.drawBottomArrow(this.getBottomPoint().add(offset), this.thickness);
        }
        else if(type === 'width'){
            //labelOffset.add(new Vector3(0, this.thickness, 0));
            this.lines.drawLeftArrow(this.getLeftPoint().add(offset), this.thickness);
            this.lines.drawRightArrow(this.getRightPoint().add(offset), this.thickness);
        }
        else if(type === 'depth'){
            //labelOffset.add(new Vector3(-this.thickness, 0, 0));
            this.lines.drawFrontArrow(this.getFrontPoint().add(offset), this.thickness);
            this.lines.drawBackArrow(this.getBackPoint().add(offset), this.thickness);
        }

        //label.position.copy(midpoint.clone().add(labelOffset));

    }

}

/**
 * @param a {Vector3}
 * @param b {Vector3}
 * @return {Vector3}
 */
function calculateMidpoint(a, b)
{
    const coords = ['x', 'y', 'z'].reduce((coords, axis) => {
        const min = Math.min(a[axis], b[axis]);
        const max = Math.max(a[axis], b[axis]);
        const diff = max - min;
        coords[axis] = min + (diff / 2);
        return coords;
    }, {});

    return new Vector3(coords.x, coords.y, coords.z);
}