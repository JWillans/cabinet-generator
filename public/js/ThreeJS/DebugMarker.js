import {Group, Mesh, MeshBasicMaterial, SphereGeometry} from "../vendor/three.module.js";

export class DebugMarker extends Group
{

    constructor()
    {
        super();

        const geometry = new SphereGeometry(0.01, 32, 16);
        const material = new MeshBasicMaterial({
            color: 0xff0000
        });
        const sphere = new Mesh(geometry, material);

        this.add(sphere);

    }

}