import {Group, PlaneGeometry, Vector3} from "../vendor/three.module.js";
import * as THREE from "../vendor/three.module.js";

export class Lines extends Group
{

    constructor()
    {
        super();

        this.material = new THREE.LineBasicMaterial( { color: 0x000000 } );

    }

    drawLine(start, end)
    {
        const geometry = new THREE.BufferGeometry().setFromPoints([start, end]);
        this.add(new THREE.Line( geometry, this.material ));
    }

    /**
     * @param origin {Vector3}
     * @param width {number}
     */
    drawHorizontalBar(origin, width)
    {
        this.drawLine(
            origin.clone().add(new Vector3(-(width / 2), 0, 0)),
            origin.clone().add(new Vector3((width / 2), 0, 0)),
        );
    }

    /**
     * @param origin {Vector3}
     * @param height {number}
     */
    drawVerticalBar(origin, height)
    {
        this.drawLine(
            origin.clone().add(new Vector3(0, -(height / 2), 0)),
            origin.clone().add(new Vector3(0, (height / 2), 0)),
        );
    }

    /**
     * @param origin {Vector3}
     * @param width {number}
     */
    drawTopArrow(origin, width)
    {
        this.drawHorizontalBar(origin, width);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(width / 3, -(width / 2), 0))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(width / 3), -(width / 2), 0))
        );
    }

    /**
     * @param origin {Vector3}
     * @param height {number}
     */
    drawLeftArrow(origin, height)
    {
        this.drawVerticalBar(origin, height);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(height / 2, height / 3, 0))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(height / 2, -(height / 3), 0))
        );

    }

    /**
     * @param origin {Vector3}
     * @param height {number}
     */
    drawRightArrow(origin, height)
    {
        this.drawVerticalBar(origin, height);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(height / 2), height / 3, 0))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(height / 2), -(height / 3), 0))
        );

    }

    /**
     * @param origin {Vector3}
     * @param width {number}
     */
    drawFrontArrow(origin, width)
    {
        this.drawHorizontalBar(origin, width);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(width / 3), 0, -(width / 2)))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3((width / 3), 0, -(width / 2)))
        );

    }

    /**
     * @param origin {Vector3}
     * @param width {number}
     */
    drawBackArrow(origin, width)
    {
        this.drawHorizontalBar(origin, width);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(width / 3), 0, (width / 2)))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3((width / 3), 0, (width / 2)))
        );

    }

    /**
     * @param origin {Vector3}
     * @param width {number}
     */
    drawBottomArrow(origin, width)
    {
        this.drawHorizontalBar(origin, width);

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(width / 3, (width / 2), 0))
        );

        this.drawLine(
            origin.clone().add(new Vector3(0, 0, 0)),
            origin.clone().add(new Vector3(-(width / 3), (width / 2), 0))
        );
    }

}