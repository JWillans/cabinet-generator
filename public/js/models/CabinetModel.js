import {State} from "../state.js";

export class CabinetModel
{

    /** @type {State} */
    state;

    constructor(parameters) {
        this.state = new State(
            {
                ...this.getDefaultParameters(),
                ...(parameters ?? {})
            }
        );
    }

    /**
     * @return {Object<string, *>}
     */
    getDefaultParameters()
    {
        throw `getDefaultParameters() not implemented for ${this.constructor.name}`
    }

    /**
     * @return {Object<string, *>}
     */
    getParameters()
    {
        return this.state.get();
    }

    getName()
    {
        throw `getName() not implemented for ${this.constructor.name}`
    }

    /**
     * @return {HTMLElement}
     */
    createForm()
    {
        throw `createForm() not implemented for ${this.constructor.name}`
    }

    /**
     * @return {CabinetComponent[]}
     */
    calculateComponents()
    {
        throw `calculateComponents() not implemented for ${this.constructor.name}`
    }

}