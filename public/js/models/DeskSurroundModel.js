import {
    calculateDividerComponents,
    calculateFrontOpenBoxComponents,
    createComponent,
    createVector3,
    offsetVector3,
    shrinkComponent,
    splitComponentVertically,
} from "../cabinet-rendering.js";

import {createCheckbox, createForm, createFormRow} from "../ui.js";
import {CabinetModel} from "./CabinetModel.js";

export class DeskSurroundModel extends CabinetModel
{

    getName()
    {
        return 'desk-surround';
    }

    getDefaultParameters()
    {
        return {
            dimensions: createVector3(2900, 2100, 840),
            desk_dimensions: createVector3(1550, 930, 650),
            desk_side_gap: 40,
            overhead_storage_height: 540,
            panel_thickness: 18,
            left_box_width: null, // number|null - null = auto calculate width
            back_cable_gap: 20, // Space behind the cavity backplate to fix cables (mm)
            overhead_light_overhang: 100, // Adds a lip below the overhead box to hide an LED strip light (mm)
            overhead_light_overhang_setback: 20,
            overhead_support_strip_thickness: 20, // Thickness of stripwood supporting overhead box (mm)
            desk_back_gap: 50, // Space between back of desk and backplate (mm) (for visualisation only)
            cavity_back_plate_height: 1220,

            hide_doors: false,
            hide_upper_side_boxes: false,
            hide_lower_side_boxes: false,
            hide_desk: false,
            hide_overhead_box: false,
            hide_overhead_supports: false,

            exploded_view: true,
        }
    }

    createForm()
    {
        const parameters = this.getParameters();

        const form = createForm();

        form.append(
            createFormRow('exploded_view', 'Exploded view', createCheckbox(
                'exploded_view',
                parameters.exploded_view,
                exploded_view => {
                    this.state.setProperty('exploded_view', exploded_view);
                }
            ))
        );

        form.append(
            createFormRow('hide_doors', 'Hide doors', createCheckbox(
                'hide_doors',
                parameters.hide_doors,
                hide_doors => {
                    this.state.setProperty('hide_doors', hide_doors);
                }
            ))
        );

        form.append(
            createFormRow('hide_upper_side_boxes', 'Hide upper side boxes', createCheckbox(
                'hide_upper_side_boxes',
                parameters.hide_upper_side_boxes,
                hide_upper_side_boxes => {
                    this.state.setProperty('hide_upper_side_boxes', hide_upper_side_boxes);
                }
            ))
        );

        form.append(
            createFormRow('hide_lower_side_boxes', 'Hide lower side boxes', createCheckbox(
                'hide_lower_side_boxes',
                parameters.hide_lower_side_boxes,
                hide_lower_side_boxes => {
                    this.state.setProperty('hide_lower_side_boxes', hide_lower_side_boxes);
                }
            ))
        );

        form.append(
            createFormRow('hide_desk', 'Hide desk', createCheckbox(
                'hide_desk',
                parameters.hide_desk,
                hide_desk => {
                    this.state.setProperty('hide_desk', hide_desk);
                }
            ))
        );

        form.append(
            createFormRow('hide_overhead_box', 'Hide overhead box', createCheckbox(
                'hide_overhead_box',
                parameters.hide_overhead_box,
                hide_overhead_box => {
                    this.state.setProperty('hide_overhead_box', hide_overhead_box);
                }
            ))
        );

        form.append(
            createFormRow('hide_overhead_supports', 'Hide overhead supports', createCheckbox(
                'hide_overhead_supports',
                parameters.hide_overhead_supports,
                hide_overhead_supports => {
                    this.state.setProperty('hide_overhead_supports', hide_overhead_supports);
                }
            ))
        );

        return form;
    }

    /**
     * @return CabinetComponent
     */
    calculateLeftBoxComponent()
    {
        const parameters = this.getParameters();

        const width = this.calculateLeftBoxWidth();
        return createComponent(
            'placeholder',
            'left_box',
            createVector3(width, parameters.dimensions.y, parameters.dimensions.z),
            createVector3(0, 0, 0),
        )
    }

    calculateLeftBoxWidth()
    {
        const parameters = this.getParameters();
        return (parameters.dimensions.x - this.calculateCavityWidth()) / 2;
    }

    calculateCavityWidth()
    {
        const parameters = this.getParameters();
        return parameters.desk_dimensions.x + (parameters.desk_side_gap * 2);
    }

    calculateRightBoxWidth()
    {
        const parameters = this.getParameters();
        return parameters.dimensions.x - this.calculateCavityWidth() - this.calculateLeftBoxWidth();
    }

    calculateRightBoxComponent()
    {
        const parameters = this.getParameters();

        const leftBox = this.calculateLeftBoxComponent();
        const width = this.calculateRightBoxWidth();

        const dimensions = leftBox.dimensions.clone().setX(width);
        const position = leftBox.position.clone().setX(parameters.dimensions.x - width);

        return createComponent(
            'placeholder',
            'right_box',
            dimensions,
            position,
        )
    }

    calculateOverheadBoxComponent()
    {
        const parameters = this.getParameters();

        const cavityWidth = this.calculateCavityWidth();
        const leftBox = this.calculateLeftBoxComponent();

        const width = cavityWidth;
        const height = parameters.overhead_storage_height;

        return createComponent(
            'placeholder',
            'overhead_box',
            createVector3(width, height, leftBox.dimensions.z),
            createVector3(leftBox.dimensions.x, parameters.dimensions.y - height, leftBox.position.z),
        );
    }

    calculatePlaceholderComponents()
    {
        const parameters = this.getParameters();

        /** @type {Vector3} */
        const deskDimensions = parameters.desk_dimensions;

        const deskOffset = createVector3(
            this.calculateLeftBoxWidth() + parameters.desk_side_gap,
            0,
            parameters.panel_thickness + parameters.back_cable_gap + parameters.desk_back_gap,
        );

        let components = [];

        if(!parameters.hide_desk){
            components = [
                ...components,
                ...this.calculateMusicDeskComponents(deskDimensions, deskOffset),
            ]
        }

        return components;
    }

    calculateDeskCavityComponents()
    {
        const parameters = this.getParameters();

        let components = [];

        const leftBox = this.calculateLeftBoxComponent();
        const overheadBox = this.calculateOverheadBoxComponent();

        const width = this.calculateCavityWidth();
        const height = parameters.dimensions.y - overheadBox.dimensions.y;
        const depth = overheadBox.dimensions.z;

        const overheadSupportLeftPosition = createVector3(
            leftBox.dimensions.x,
            parameters.dimensions.y - overheadBox.dimensions.y - parameters.overhead_support_strip_thickness,
            parameters.back_cable_gap + parameters.panel_thickness
        );

        const overheadSupportRightPosition = createVector3(
            leftBox.dimensions.x + overheadBox.dimensions.x - parameters.overhead_support_strip_thickness,
            parameters.dimensions.y - overheadBox.dimensions.y - parameters.overhead_support_strip_thickness,
            parameters.back_cable_gap + parameters.panel_thickness
        );

        const overheadSupportDimensions = createVector3(
            parameters.overhead_support_strip_thickness,
            parameters.overhead_support_strip_thickness,
            parameters.dimensions.z - parameters.back_cable_gap - parameters.panel_thickness - parameters.overhead_light_overhang_setback - parameters.panel_thickness
        );

        components.push(createComponent(
            'attachment',
            'overhead_left_support',
            overheadSupportDimensions,
            overheadSupportLeftPosition,
        ));

        components.push(createComponent(
            'attachment',
            'overhead_right_support',
            overheadSupportDimensions,
            overheadSupportRightPosition,
        ));

        const cavity = createComponent(
            'placeholder',
            'desk_cavity',
            createVector3(width, height, depth),
            createVector3(leftBox.position.x + leftBox.dimensions.x, leftBox.position.y, leftBox.position.z),
        );
        //components.push(cavity); // @debug

        const backPlateHeight = parameters.cavity_back_plate_height;
        components.push(createComponent(
            'panel',
            'desk_cavity_back_plate',
            createVector3(cavity.dimensions.x, backPlateHeight, parameters.panel_thickness),
            createVector3(cavity.position.x, cavity.position.y + (cavity.dimensions.y - backPlateHeight), cavity.position.z + parameters.back_cable_gap),
        ));

        if(parameters.overhead_light_overhang > 0){

            const overhangDimensions = createVector3(cavity.dimensions.x, parameters.overhead_light_overhang, parameters.panel_thickness);

            const overhangPosition = createVector3(
                cavity.position.x,
                cavity.position.y + cavity.dimensions.y - overhangDimensions.y,
                cavity.position.z + cavity.dimensions.z - overhangDimensions.z - parameters.overhead_light_overhang_setback
            );

            components.push(createComponent(
                'panel',
                'overhead_light_overhang',
                overhangDimensions,
                overhangPosition,
            ))

        }

        return components;

    }

    /**
     * @param dimensions {Vector3}
     * @param position {Vector3}
     * @return {CabinetComponent[]}
     */
    calculateMusicDeskComponents(dimensions, position)
    {
        const monitorShelfTop = dimensions.y;
        const monitorShelfDepth = 200;

        const desktopTop = 780;

        const keyboardShelfTop = 630;
        const keyboardShelfDepth = 275;
        const keyboardShelfSetback = 50;

        const supportInset = 25;

        const panelThickness = 20;

        const supportDimensions = createVector3(panelThickness, desktopTop - panelThickness, dimensions.z);
        const supportLeftPosition = createVector3(position.x + supportInset, position.y, position.z);
        const supportRightPosition = createVector3(position.x + dimensions.x - supportInset - panelThickness, position.y, position.z);

        const monitorShelfBox = createComponent(
            'placeholder',
            'monitor_shelf_box',
            createVector3(dimensions.x, monitorShelfTop - desktopTop, monitorShelfDepth),
            createVector3(position.x, position.y + desktopTop, position.z)
        );

        const monitorShelfBoxInner = createComponent(
            'placeholder',
            'monitor_shelf_box_inner',
            createVector3(monitorShelfBox.dimensions.x - (panelThickness * 2), monitorShelfBox.dimensions.y - panelThickness, monitorShelfBox.dimensions.z),
            createVector3(monitorShelfBox.position.x + panelThickness, monitorShelfBox.position.y, monitorShelfBox.position.z)
        );

        const monitorShelfDimensions = createVector3(monitorShelfBox.dimensions.x, panelThickness, monitorShelfBox.dimensions.z);
        const monitorShelfPosition = createVector3(monitorShelfBox.position.x, monitorShelfBox.position.y + monitorShelfBox.dimensions.y - panelThickness, monitorShelfBox.position.z);

        const monitorShelfSupportDimensions = createVector3(panelThickness, monitorShelfBox.dimensions.y - panelThickness, monitorShelfBox.dimensions.z);
        const monitorShelfLeftSupportPosition = createVector3(position.x, position.y + desktopTop, position.z);

        const monitorShelfRightSupportPosition = monitorShelfLeftSupportPosition.clone().setX(monitorShelfLeftSupportPosition.x + dimensions.x - panelThickness);

        const widthBetweenSupports = dimensions.x - (supportLeftPosition.x + panelThickness) - (dimensions.x - supportRightPosition.x);

        const keyboardShelfDimensions = createVector3(widthBetweenSupports, panelThickness, keyboardShelfDepth);
        const keyboardShelfPosition = createVector3(supportLeftPosition.x + panelThickness, position.y + keyboardShelfTop - panelThickness, position.z + dimensions.z - keyboardShelfSetback - keyboardShelfDimensions.z);

        let components = [
            ...calculateDividerComponents(monitorShelfBoxInner, panelThickness, 'desk_monitor_shelf', 2).map(component => {
                component.type = 'placeholder';
                return component;
            })
        ];

        components.push(createComponent(
            'placeholder',
            'desk_top',
            createVector3(dimensions.x, panelThickness, dimensions.z),
            createVector3(position.x, position.y + desktopTop - panelThickness, position.z),
        ));

        components.push(createComponent(
            'placeholder',
            'desk_left_support',
            supportDimensions.clone(),
            supportLeftPosition,
        ));

        components.push(createComponent(
            'placeholder',
            'desk_right_support',
            supportDimensions.clone(),
            supportRightPosition,
        ));

        components.push(createComponent(
            'placeholder',
            'desk_monitor_shelf',
            monitorShelfDimensions,
            monitorShelfPosition,
        ));

        components.push(createComponent(
            'placeholder',
            'desk_monitor_shelf_left_support',
            monitorShelfSupportDimensions,
            monitorShelfLeftSupportPosition,
        ));

        components.push(createComponent(
            'placeholder',
            'desk_monitor_shelf_right_support',
            monitorShelfSupportDimensions,
            monitorShelfRightSupportPosition,
        ));

        components.push(createComponent(
            'placeholder',
            'desk_keyboard_shelf',
            keyboardShelfDimensions,
            keyboardShelfPosition,
        ));

        return components;
    }

    calculateMonitorClearanceComponent()
    {
        const parameters = this.getParameters();

        const cavityWidth = this.calculateCavityWidth();
        const clearanceHeight = parameters.dimensions.y - parameters.desk_dimensions.y - parameters.overhead_storage_height;

        const dimensions = createVector3(cavityWidth, clearanceHeight, 250);
        const position = createVector3(this.calculateLeftBoxWidth(), parameters.desk_dimensions.y, 0);

        return createComponent(
            'info',
            'monitor_clearance',
            dimensions,
            position,
            undefined,
            'Space available for mounting a monitor against the back plate'
        );
    }

    calculateComponents()
    {
        const parameters = this.getParameters();

        const leftBox = this.calculateLeftBoxComponent();
        const rightBox = this.calculateRightBoxComponent();
        const overheadBox = this.calculateOverheadBoxComponent();

        const leftBoxInner = {...shrinkComponent(leftBox, parameters.panel_thickness * 2), id: 'left_box_internal', type: 'info', description: 'Left box internal dimensions'};
        const rightBoxInner = {...shrinkComponent(rightBox, parameters.panel_thickness * 2), id: 'right_box_internal', type: 'info', description: 'Right box internal dimensions'};
        const overheadBoxInner = {...shrinkComponent(overheadBox, parameters.panel_thickness * 2), id: 'overhead_internal', type: 'info', description: 'Overhead space internal dimensions'};

        let components = [
            leftBoxInner,
            rightBoxInner,
            overheadBoxInner,
            this.calculateMonitorClearanceComponent(),
            ...this.calculatePlaceholderComponents(),
        ];

        if(!parameters['hide_overhead_supports']){
            components = [
                ...components,
                ...this.calculateDeskCavityComponents(),
            ];
        }

        if(!parameters.hide_overhead_box){
            components = [
                ...components,
                ...calculateFrontOpenBoxComponents(overheadBox, parameters, 'overhead'),
            ];
        }

        if(!parameters.hide_side_boxes){

            const sideBoxOptions = {
                num_doors: parameters.hide_doors ? 0 : 1,
            }

            const sideBoxStacks = [
                splitComponentVertically(leftBox, 2),
                splitComponentVertically(rightBox, 2)
            ];

            sideBoxStacks.forEach((stack) => {
                stack.forEach((stackBox, index) => {

                    if(index === 0 && parameters.hide_lower_side_boxes) return;
                    if(index > 0 && parameters.hide_upper_side_boxes) return;

                    components = [
                        ...components,
                        ...calculateFrontOpenBoxComponents(stackBox, parameters, stackBox.id, {
                            ...sideBoxOptions,
                            bottom_plate: index === 0,
                            num_shelves: index > 0 ? 1 : 0,
                        }),
                    ]
                });
            });

        }

        if(parameters.exploded_view){

            const explodeDistance = 400;

            components = components.map((component) => {

                const id = component.id;

                if(component.type === 'placeholder') return component;

                let offset = createVector3(0, 0 ,0);

                if(id.match(/^left_box_/)){
                    offset.x -= explodeDistance;
                }

                if(id.match(/^right_box_/)){
                    offset.x += explodeDistance;
                }

                if(id.match(/_box_2_/)){
                    offset.y += explodeDistance;
                }

                if(id.match(/^overhead_/)){
                    offset.y += explodeDistance;
                }

                if(id.match(/^overhead_light/)){
                    offset.z += explodeDistance;
                    offset.y -= explodeDistance;
                }

                if(id.match(/_door_/)){
                    offset.z += explodeDistance;
                }

                if(id.match(/_back_plate$/)){
                    offset.z -= explodeDistance;
                }

                if(id.match(/_support$/)){
                    offset.y -= explodeDistance;
                }

                component.position = offsetVector3(component.position, offset);

                return component;
            })
        }

        return components;

    }

}