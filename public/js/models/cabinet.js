import {createComponent, createVector3, offsetVector3} from "../cabinet-rendering.js";
import {createCheckbox, createForm, createFormRow, createInput, createSelect} from "../ui.js";
import {createElement} from "../dom.js";
import {State} from "../state.js";

/**
 * @type {CabinetParameters}
 */
export const DEFAULT_CABINET_PARAMETERS = {
    dimensions: createVector3(600, 900, 570),
    panel_thickness: 18,
    feet_dimensions: createVector3(50, 100, 50),
    feet_offset: 25,
    door_type: "single_left",
    door_inset: true,
    door_gap: 2,
    back_overhang: 30,
}

/**
 * @param [unresolvedParameters] {CabinetParameters}
 * @return CabinetModel
 */
export function createCabinetModel(unresolvedParameters)
{
    const parameters = {...DEFAULT_CABINET_PARAMETERS, ...(unresolvedParameters ?? {})}
    const state = new State(parameters, undefined, 'cabinet_parameters');

    return {
        parameters: parameters,
        state: state,
        form: renderCabinetParametersForm(state),
        component_calculator: calculateCabinetComponents,
    }
}

/**
 * @param parametersState {State}
 * @return {HTMLFormElement}
 */
function renderCabinetParametersForm(parametersState)
{

    let children = [];

    /** @type {CabinetParameters} */
    const parameters = parametersState.get();

    [['Width (mm)', 'x'], ['Height (mm)', 'y'], ['Depth (mm)', 'z']].forEach(entry => {
        children.push(createFormRow(`dimensions_${entry[1]}`, entry[0], createInput(
            `dimensions_${entry[1]}`,
            'number',
            parametersState.getProperty('dimensions')[entry[1]],
            value => {
                const dimensions = {...parametersState.getProperty('dimensions'), [entry[1]]: value ?? 0};
                parametersState.setProperty('dimensions', dimensions);
            }
        )));
    })

    children = [
        ...children,
        createFormRow('panel_thickness', 'Panel thickness (mm)', createInput(
            'panel_thickness',
            'number',
            parameters.panel_thickness,
            panel_thickness => {
                parametersState.setProperty('panel_thickness', panel_thickness ?? 0);
            }
        )),
        createFormRow('panel_thickness', 'Back overhang (mm)', createInput(
            'back_overhang',
            'number',
            parameters.back_overhang,
            back_overhang => {
                parametersState.setProperty('back_overhang', back_overhang ?? 0);
            }
        )),
        createFormRow('feet_height', 'Feet height (mm)', createInput(
            'feet_height',
            'number',
            parameters.feet_dimensions.y,
            feet_height => {
                const dimensions = {...parametersState.getProperty('feet_dimensions'), y: feet_height}
                parametersState.setProperty('feet_dimensions', dimensions);
            }
        )),
        createFormRow('feet_offset', 'Feet offset (mm)', createInput(
            'feet_offset',
            'number',
            parameters.feet_offset,
            feet_offset => {
                parametersState.setProperty('feet_offset', feet_offset ?? 0);
            }
        )),
        createFormRow('door_inset', 'Doors inset', createCheckbox(
            'door_inset',
            parameters.door_inset,
            door_inset => {
                parametersState.setProperty('door_inset', door_inset);
            }
        )),
        createFormRow('door_gap', 'Door gap (mm)', createInput(
            'door_gap',
            'number',
            parameters.door_gap,
            door_gap => {
                parametersState.setProperty('door_gap', door_gap ?? 0);
            }
        )),
        createFormRow('door_type', 'Doors', createSelect(
            'door_type',
            parameters.door_type,
            [
                {label: "None", value: "none"},
                {label: "Double", value: "double"},
                {label: "Single (left hinged)", value: "single_left"},
                {label: "Singe (right hinged)", value: "single_right"}
            ],
            door_type => {
                parametersState.setProperty('door_type', door_type ?? "none");
            }
        )),
    ];

    const form = createForm(children);

    return createElement('div', {
        children: [form]
    });
}

/**
 * @param {CabinetParameters} parameters
 * @return {CabinetComponent[]}
 */
export function calculateCabinetComponents(parameters)
{
    parameters = {...DEFAULT_CABINET_PARAMETERS, ...parameters};

    return [
        ...calculateCabinetInfoComponents(parameters),
        ...calculateCabinetDoorComponents(parameters),
        ...calculateCabinetBoxComponents(parameters),
        ...calculateCabinetFeetComponents(parameters),
    ];
}

/**
 * @param parameters {CabinetParameters}
 * @return {CabinetComponent[]}
 */
function calculateCabinetFeetComponents(parameters)
{
    const dimensions = parameters.feet_dimensions;
    const offset = parameters.feet_offset;

    if(dimensions.y <= 0) return [];

    let components = [];

    const outer = calculateCabinetBoxOuter(parameters);

    components.push(createComponent(
        'attachment',
        'foot_1',
        dimensions,
        createVector3(offset, 0, offset)
    ));

    components.push(createComponent(
        'attachment',
        'foot_2',
        dimensions,
        createVector3(offset, 0, outer.dimensions.z - offset - dimensions.z)
    ));

    components.push(createComponent(
        'attachment',
        'foot_3',
        dimensions,
        createVector3(parameters.dimensions.x - dimensions.x - offset, 0, outer.dimensions.z - offset - dimensions.z)
    ));

    components.push(createComponent(
        'attachment',
        'foot_4',
        dimensions,
        createVector3(parameters.dimensions.x - dimensions.x - offset, 0, offset)
    ));

    return components;
}

/**
 * @param parameters {CabinetParameters}
 * @return {{origin: Vector3, dimensions: Vector3}}
 */
function calculateCabinetBoxInner(parameters)
{
    const outer = calculateCabinetBoxOuter(parameters);

    const width = outer.dimensions.x - (parameters.panel_thickness * 2);
    const height = outer.dimensions.y - (parameters.panel_thickness * 2);
    const depth = outer.dimensions.z - parameters.panel_thickness - (parameters.door_inset ? parameters.panel_thickness : 0);

    const dimensions = createVector3(width, height, depth);

    const origin = offsetVector3(outer.origin, createVector3(
        parameters.panel_thickness,
        parameters.panel_thickness,
        parameters.panel_thickness
    ));

    return {
        dimensions: dimensions,
        origin: origin,
    }
}

function calculateCabinetBoxOuter(parameters)
{
    const width = parameters.dimensions.x;
    const height = parameters.dimensions.y - parameters.feet_dimensions.y;
    const depth = parameters.dimensions.z - parameters.back_overhang - (parameters.door_inset ? 0 : parameters.panel_thickness);

    const dimensions = createVector3(width, height, depth);

    const origin = createVector3(0, parameters.feet_dimensions.y, 0);

    return {
        dimensions: dimensions,
        origin: origin,
    }
}

/**
 * @param parameters {CabinetParameters}
 * @return {CabinetComponent[]}
 */
function calculateCabinetDoorComponents(parameters)
{
    let components = [];

    const type = parameters.door_type;

    if(type === 'none') return components;

    const inner = calculateCabinetBoxInner(parameters);
    const outer = calculateCabinetBoxOuter(parameters);

    const inset = parameters.door_inset;

    const bounds = inset ? inner : outer;

    const gap = parameters.door_gap;

    const doorWidth = (
        inset ?
        (
            type === "double" ?
                (inner.dimensions.x / 2) - gap - (gap * .5) :
                (inner.dimensions.x) - (gap * 2)
        ) :
        (
            type === "double" ?
                (outer.dimensions.x / 2) - (gap / 2) :
                (outer.dimensions.x)
        )
    );

    const doorHeight = (
        inset ?
        inner.dimensions.y - (gap * 2) :
        outer.dimensions.y
    );

    const doorOffset = inset ? gap : 0;
    const doorDimensions = createVector3(doorWidth, doorHeight, parameters.panel_thickness);

    /** @type {Vector3[]} */
    let handlePositions = [];

    const handleDimensions = createVector3(15, 100, 25);
    const handleOffsetX = 30;
    const handleOffsetY = (doorDimensions.y / 2) - (handleDimensions.y / 2);

    if(type === "single_left" || type === "double")
    {
        const leftDoorPosition = createVector3(
            bounds.origin.x + doorOffset,
            bounds.origin.y + doorOffset,
            bounds.origin.z + bounds.dimensions.z + (inset ? 0 : gap)
        );

        handlePositions.push(offsetVector3(leftDoorPosition, createVector3(
            doorDimensions.x - handleDimensions.x - handleOffsetX,
            handleOffsetY,
            parameters.panel_thickness
        )));

        components.push(createComponent(
            'panel',
            'left_hinged_door',
            doorDimensions,
            leftDoorPosition,
        ))
    }

    if(type === "single_right" || type === "double")
    {

        const rightDoorPosition = createVector3(
            bounds.origin.x + bounds.dimensions.x - doorDimensions.x - (type === "double" && !inset ? 0 : gap),
            bounds.origin.y + doorOffset,
            bounds.origin.z + bounds.dimensions.z + (inset ? 0 : gap)
        );

        handlePositions.push(offsetVector3(rightDoorPosition, createVector3(
            handleOffsetX,
            handleOffsetY,
            parameters.panel_thickness
        )));

        components.push(createComponent(
            'panel',
            'right_hinged_door',
            doorDimensions,
            rightDoorPosition,
        ))
    }

    handlePositions.forEach((handlePosition, handleIndex) => {
        components.push(createComponent(
            'attachment',
            `handle_${handleIndex + 1}`,
            handleDimensions,
            handlePosition,
        ));
    });

    return components;

}

function calculateCabinetInfoComponents(parameters)
{
    const inner = calculateCabinetBoxInner(parameters);

    /** @type {CabinetComponent[]} */
    let components = [];

    components.push(createComponent(
        'info',
        'interior',
        inner.dimensions,
        inner.origin,
        null,
        'Interior dimensions of cabinet'
    ));

    return components;
}

function calculateCabinetBoxComponents(parameters)
{
    const outer = calculateCabinetBoxOuter(parameters);
    const inner = calculateCabinetBoxInner(parameters);

    /** @type {CabinetComponent[]} */
    let components = [];

    components.push(createComponent(
        'panel',
        'box_top',
        createVector3(
            outer.dimensions.x,
            parameters.panel_thickness,
            outer.dimensions.z + parameters.back_overhang
        ),
        offsetVector3(outer.origin, createVector3(
            0,
            outer.dimensions.y - parameters.panel_thickness,
            -(parameters.back_overhang)
        ))
    ));

    components.push(createComponent(
        'panel',
        'box_bottom',
        createVector3(
            outer.dimensions.x,
            parameters.panel_thickness,
            outer.dimensions.z
        ),
        outer.origin
    ));

    components.push(createComponent(
        'panel',
        'box_left',
        createVector3(
            parameters.panel_thickness,
            outer.dimensions.y - (parameters.panel_thickness * 2),
            outer.dimensions.z
        ),
        offsetVector3(outer.origin, createVector3(0, parameters.panel_thickness, 0))
    ));

    components.push(createComponent(
        'panel',
        'box_right',
        createVector3(
            parameters.panel_thickness,
            outer.dimensions.y - (parameters.panel_thickness * 2),
            outer.dimensions.z
        ),
        offsetVector3(
            outer.origin,
            createVector3(
                outer.dimensions.x - parameters.panel_thickness,
                parameters.panel_thickness,
                0
            )
        ),
    ));

    components.push(createComponent(
        'panel',
        'box_back',
        createVector3(
            inner.dimensions.x,
            inner.dimensions.y,
            parameters.panel_thickness,
        ),
        offsetVector3(inner.origin, createVector3(0, 0, -parameters.panel_thickness)),
    ));

    return components;
}